Ignores:
1. Local .terraform directories on any depth
2. Files which names end with '.tfstate'
3. Files which contain '.tfstate.' in names
4. Files with full names 'crash.log'
5. Files which names start with 'crash.' and end with '.log'
6. Files which names end with the '.tfvars'
7. Files with full names 'override.tf' and 'override.tf.json'
8. Files which names end with '_override.tf' or '_override.tf.json'

Doesn't isnore:
1. File with the name 'example_override.tf'

Ignores:
9. Files with names '.terraformrc', 'terraform.rc'
